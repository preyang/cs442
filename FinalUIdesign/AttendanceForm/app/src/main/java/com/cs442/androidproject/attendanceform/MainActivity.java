package com.cs442.androidproject.attendanceform;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyUserCallback;
import com.kinvey.java.User;

public class MainActivity extends Activity {

    final static String LOGTAG = "ATF";
    private Client mAttendanceFormClient;

    public static Users currentUser = new Users();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
        //check if someone is aready logon on the system
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().retrieve(new KinveyUserCallback() {
            @Override
            public void onSuccess(User user) {
                setProgressBarIndeterminateVisibility(false);
                Log.d(LOGTAG, user.getUsername());
                //TODO Zhou - this is where i get the username of the current user - you can use it if you have need for it
                //TODO Zhou - here determine if current user is a professor or student using the userstatus and navigate to the approriate screen.

                String userStatus;

                //navigating to prof dashbaord for test purpose
                Intent intent_prof = new Intent(MainActivity.this, LoginActivity.class);
                intent_prof.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent_prof.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent_prof);

            }

            @Override
            public void onFailure(Throwable throwable) {
                setProgressBarIndeterminateVisibility(false);
                Log.d(LOGTAG, "Kinvey failure : " + throwable.getMessage());
                NavigatetoLogin();
            }
        });
    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            setProgressBarIndeterminateVisibility(true);
            mAttendanceFormClient.user().logout().execute();
            Log.d(LOGTAG, "User logged out");
            setProgressBarIndeterminateVisibility(false);
            NavigatetoLogin();
        }
        return super.onOptionsItemSelected(item);
    }

    public static Users getCurrentUser(){
        return currentUser;
    }
}

/*
attedance,absent,grade,profile,notification,class screem for professor
 */