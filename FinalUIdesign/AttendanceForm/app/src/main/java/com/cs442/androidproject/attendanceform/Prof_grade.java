package com.cs442.androidproject.attendanceform;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.cs442.androidproject.attendanceform.Model.Classes;
import com.cs442.androidproject.attendanceform.Model.StudentGrade;
import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.java.core.KinveyClientCallback;

public class Prof_grade extends Activity {

    Users currentUser = MainActivity.getCurrentUser();
    Classes currentClass = ProfDashboard.getCurrentClass();
    final static String LOGTAG = "ATF";

    private Client mAttendanceFormClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prof_grade_student);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
    }

    private void newGrade(int ngrade){
        StudentGrade studentGrade = new StudentGrade();
        studentGrade.setGrade(String.valueOf(ngrade));
        studentGrade.setStudent_email(currentUser.getStudent_email());
        studentGrade.setDate("");//something here
        studentGrade.setCourse_code(currentClass.getCourse_code());
        AsyncAppData<StudentGrade> newstudentgrade = mAttendanceFormClient.appData("Student_Grades", StudentGrade.class);
        newstudentgrade.save(studentGrade, new KinveyClientCallback<StudentGrade>() {
            @Override
            public void onSuccess(StudentGrade studentGrade) {
                Log.i(LOGTAG, "Grade succesfully entered!");
                AlertDialog.Builder builder= new AlertDialog.Builder(Prof_grade.this);
                builder.setMessage(R.string.grading_succes_label)
                        .setTitle(R.string.grading_succes_title)
                        .setPositiveButton(android.R.string.ok, null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.i(LOGTAG, throwable.getMessage());
            }
        });

    }


}
