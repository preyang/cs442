package com.cs442.androidproject.attendanceform;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;


public class Student_attendance extends AppCompatActivity {
    CheckBox ontime, late;
    Button submit;
    Button btn1;
    EditText reason;
    TextView gps;
    private LocationManager lm;
    private LocationListener ll;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configureButton();
                return;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_attendance);
        ontime = (CheckBox) findViewById(R.id.checkBox_attedence);
        late = (CheckBox) findViewById(R.id.checkBox_late);
        submit = (Button) findViewById(R.id.submit_student);
      //  btn1= (Button)findViewById(R.id.button1);
        reason = (EditText) findViewById(R.id.editText);
        gps = (TextView) findViewById(R.id.textViewgps);

        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        ll = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Double lat = location.getLatitude();
                Double lon = location.getLongitude();
/*it prints latitude of location*/
                if (location != null) {

                    Log.e("GPS", "location changed: lat=" + lat + ", lon=" + lon);
                    gps.setText(String.valueOf(lat));
                    // gps.setText(String.valueOf(lon));
                } else {
                    gps.setText("Provider not available");
                    //gps.setText("Provider not available");
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            //USed when GPS is off
            @Override
            public void onProviderDisabled(String provider) {
                Intent i1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i1);
            }
        };
        //Check for permission as SDK
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET
            }, 10);
        } else {
            configureButton();
        }
        ;


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void submit_student(View v) {
        if (ontime.isChecked() && late.isChecked())
            Toast.makeText(Student_attendance.this, "Choose only one please, teacher will confuse.", Toast.LENGTH_LONG).show();
        else if (late.isChecked()) {
            //TODO deal with the reason
            String Reason = reason.getText().toString();
            Toast.makeText(Student_attendance.this, "Sumbmit success", Toast.LENGTH_LONG).show();
            finish();
        } else if (ontime.isChecked()) {
            Toast.makeText(Student_attendance.this, "Sumbmit success", Toast.LENGTH_LONG).show();
            finish();
        } else
            Toast.makeText(Student_attendance.this, "Make choice in the box", Toast.LENGTH_LONG).show();

    }
    private void configureButton() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // first parameter provider, then refresh interval, then min distance that can be used for refresh location automatically
                // when  you move  this distance, location listner
                lm.requestLocationUpdates("gps", 5000, 0, ll);
            }
        });

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Student_attendance Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
