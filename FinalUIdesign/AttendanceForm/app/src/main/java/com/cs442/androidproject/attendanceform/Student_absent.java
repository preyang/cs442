package com.cs442.androidproject.attendanceform;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Student_absent extends Activity {
    CheckBox absent;
    Button submit;
    EditText reason;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.absent_student);
        absent = (CheckBox)findViewById(R.id.checkBox_absent);
        submit=(Button)findViewById(R.id.button);
        reason=(EditText)findViewById(R.id.editText);
        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //TODO deal with the reason
                if(absent.isChecked()){
                    String Reason = reason.getText().toString();
                    Toast.makeText(Student_absent.this, "Sumbmit success", Toast.LENGTH_LONG ).show();

                }
                else{
                    Toast.makeText(Student_absent.this, "make sure in the box", Toast.LENGTH_LONG ).show();
                }
                finish();
            }
        });
    }
}
