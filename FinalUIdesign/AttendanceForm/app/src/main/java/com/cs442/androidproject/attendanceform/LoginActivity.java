package com.cs442.androidproject.attendanceform;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyUserCallback;
import com.kinvey.java.User;

public class LoginActivity extends AppCompatActivity {

    protected TextView mSignUp;
    Button log;
    private EditText user,password;
    final static String LOGTAG = "ATF";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set up Progressbar
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_login);
        user = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        mSignUp = (TextView)findViewById(R.id.tvSignUp);
        mSignUp.setOnClickListener(new View.OnClickListener(){
           @Override
            public void onClick(View v){
               Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
               startActivity(intent);
           }
        });
        //TODO added by dennis
        final Client mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();

        final RadioButton professor= (RadioButton)findViewById(R.id.radioProfessor);
        final RadioButton student = (RadioButton)findViewById(R.id.radioStudent);
        log=(Button)findViewById(R.id.login);
        log.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String username = user.getText().toString().trim();
                String Password = password.getText().toString().trim();

                //user.setText("");
                //password.setText("");
                //TODO start of - added by dennis
                if(username.isEmpty() || Password.isEmpty() ){
                    AlertDialog.Builder builder= new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage(R.string.login_error_message)
                            .setTitle(R.string.login_error_Title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else{
                    setProgressBarIndeterminateVisibility(true);
                    mAttendanceFormClient.user().login(username, Password, new KinveyUserCallback() {
                        @Override
                        public void onSuccess(User user) {
                            setProgressBarIndeterminateVisibility(false);
                            //TODO try if status of the user can be stored ub database and returned
                            //TODO Logic by Zhou - slight edit by dennis
                            //check the imformation
                            if(student.isChecked()){
                                Toast.makeText(LoginActivity.this, "hello student", Toast.LENGTH_LONG ).show();
                                Intent intent_stu = new Intent(LoginActivity.this, StudentDashboard.class);
                                intent_stu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent_stu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent_stu);
                            }
                            else {
                                Toast.makeText(LoginActivity.this, "hello professor", Toast.LENGTH_LONG ).show();
                                Intent intent_prof = new Intent(LoginActivity.this, ProfDashboard.class);
                                intent_prof.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent_prof.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent_prof);
                            }
                        }
                        @Override
                        public void onFailure(Throwable throwable) {
                            Log.d(LOGTAG, "KInvey Error - " + throwable.toString());
                            setProgressBarIndeterminateVisibility(false);
                            CharSequence text = "Username already exists.";
                            Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.show();
                        }
                    });
                }
            }
        });
    }
}
