package com.cs442.androidproject.attendanceform;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.cs442.androidproject.attendanceform.Model.Classes;
import com.kinvey.android.Client;


public class ProfDashboard extends AppCompatActivity {

    Button prof_absent,prof_attendance,prof_grade;
    private Client mAttendanceFormClient;
    final static String LOGTAG = "ATF";

    public static Classes currentClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.prof_dashboard);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();

        prof_absent=(Button)findViewById(R.id.absent_professor);
        prof_attendance=(Button)findViewById(R.id.attendence_professor);
        prof_grade=(Button)findViewById(R.id.grade_professor);
        prof_absent.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(ProfDashboard.this, Prof_absent.class);
                startActivity(intent);
            }
        });
        prof_attendance.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(ProfDashboard.this, Prof_attendance.class);
                startActivity(intent);
            }
        });
        prof_grade.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(ProfDashboard.this, Prof_grade.class);
                startActivity(intent);
            }
        });

    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            setProgressBarIndeterminateVisibility(true);
            mAttendanceFormClient.user().logout().execute();
            Log.d(LOGTAG, "User logged out");
            setProgressBarIndeterminateVisibility(false);
            NavigatetoLogin();
        }
        if(itemId == R.id.profile)
        {
            Intent i1= new Intent(ProfDashboard.this,professor_profile_screen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }


    public static Classes getCurrentClass(){
        return currentClass;
    }

}
